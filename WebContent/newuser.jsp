<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="entities.User" %>
<%@ page import="services.UserService" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>New User</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="formtemplate/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="formtemplate/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="formtemplate/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="formtemplate/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="formtemplate/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="formtemplate/css/util.css">
	<link rel="stylesheet" type="text/css" href="formtemplate/css/main.css">
<!--===============================================================================================-->
		
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="templatefiles/css/style.css" >

</head>
<body>
<header>
			<div class="wrapper d-flex align-items-stretch">
				<nav id="sidebar">
		  			<div class="img bg-wrap text-center py-4" style="background-image:url(https://scontent.ftun5-1.fna.fbcdn.net/v/t1.6435-9/67201933_895556720809180_1409536093916758016_n.png?_nc_cat=107&ccb=1-5&_nc_sid=973b4a&_nc_ohc=1hWzaeeOVawAX-zHAU8&_nc_ht=scontent.ftun5-1.fna&oh=2308c08ad6cc41c84f71b1f65567e4e6&oe=616DE895);">
		  				<div class="user-logo">
		  					<div class="img" style="background-image: url(templatefiles/images/logo.jpg);">
							</div>
		  						<h3><%
									User u = (User)session.getAttribute("user");
									out.println(u.getPrenom()+" "+u.getNom());
								%></h3>
		  				</div>
		  				</div>
	        			<ul class="list-unstyled components mb-5">
	          				<li class="active">
	            				<a href="acceuil"><span class="fa fa-home mr-3"></span>Home</a>
	          				</li>
	          				<li>
	            				<a href="#"><span class="fa fa-user mr-3"></span> Profile</a>
	          				</li>
	          				<li>
	            				<a href="list"><span class="fa fa-users mr-3"></span> Users</a>
	          				</li>
							<li>
	            				<a href="newuser"><span class="fa fa-user-plus mr-3"></span> Add User</a>
	          				</li>
	          				<li>
	            				<a href="login"><span class="fa fa-sign-out mr-3"></span> Sign Out</a>
	          				</li>
	        			</ul>
	    	    </nav>
	    	    
	        	<!-- Page Content  -->
	      		<div class="bg-contact3" style="background-image: url('');">
		<div class="container-contact3">
			<div class="wrap-contact3">
				<form class="contact3-form validate-form" action="save.do" method="post" name="newuser">
					<span class="contact3-form-title">
						New User
					</span>

					<div class="wrap-input3 validate-input" data-validate="First Name is required">
						<input class="input3" type="text" name="name" placeholder="First Name">
						<span class="focus-input3"></span>
					</div>
					
					<div class="wrap-input3 validate-input" data-validate="Last Name is required">
						<input class="input3" type="text" name="name" placeholder="Last Name">
						<span class="focus-input3"></span>
					</div>
					
					<div class="wrap-input3 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input3" type="text" name="email" placeholder="Email">
						<span class="focus-input3"></span>
					</div>

					<div class="wrap-input3 validate-input" data-validate="Category is required">
						<div>
							<select class="selection-2" name="category">
								<option>Guest</option>
								<option>User</option>
								<option>Administrator</option>
							</select>
						</div>
						<span class="focus-input3"></span>
					</div>

					<div class="wrap-input3 validate-input" data-validate="Password is required">
						<input class="input3" type="text" name="password" placeholder="Password">
						<span class="focus-input3"></span>
					</div>

					<!-- <div class="wrap-input3 validate-input" data-validate="Password Confirmation is required">
						<input class="input3" type="text" name="password_confirmation" placeholder="Confirm Password">
						<span class="focus-input3"></span>
					</div> -->

					<div class="wrap-input3 validate-input" data-validate="Photo URL is required">
						<input class="input3" type="text" name="Photo" placeholder="Photo URL">
						<span class="focus-input3"></span>
					</div>

					<div class="container-contact3-form-btn">
						<button type="submit" class="contact3-form-btn">
							Save
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="formtemplate/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="formtemplate/vendor/bootstrap/js/popper.js"></script>
	<script src="formtemplate/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="formtemplate/vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="formtemplate/js/main.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
	      		</div>
			</div>
	
	    <script type="text/javascript" src="templatefiles/js/jquery.min.js"></script>
	    <script type="text/javascript" src="templatefiles/js/popper.js"></script>
	    <script type="text/javascript" src="templatefiles/js/bootstrap.min.js"></script>
	    <script  type="text/javascript" src="templatefiles/js/main.js"></script>
			</div>
	
	    <script type="text/javascript" src="templatefiles/js/jquery.min.js"></script>
	    <script type="text/javascript" src="templatefiles/js/popper.js"></script>
	    <script type="text/javascript" src="templatefiles/js/bootstrap.min.js"></script>
	    <script  type="text/javascript" src="templatefiles/js/main.js"></script>
	  </header>
	

</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="entities.User" %>
    <%@ page import="entities.ListUser" %>
    <%@ page import="java.util.List" %>
	<%@ page import="services.UserService" %>	
    
<!doctype html>
<html lang="en">
  <head>
  	<title>Users List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="tabletemplate/css/style.css">
	<link rel="stylesheet" href="templatefiles/css/style.css" >

	</head>
	<body>
	<header>
			<div class="wrapper d-flex align-items-stretch">
				<nav id="sidebar">
		  			<div class="img bg-wrap text-center py-4" style="background-image: 
                         url(https://scontent.ftun5-1.fna.fbcdn.net/v/t1.6435-9/67201933_895556720809180_1409536093916758016_n.png?_nc_cat=107&ccb=1-5&_nc_sid=973b4a&_nc_ohc=1hWzaeeOVawAX-zHAU8&_nc_ht=scontent.ftun5-1.fna&oh=2308c08ad6cc41c84f71b1f65567e4e6&oe=616DE895);">
		  				<div class="user-logo">
		  					<div class="img" style="background-image: url(templatefiles/images/logo.jpg);">
							</div>
		  						<h3><%
									User u = (User)session.getAttribute("user");
									out.println(u.getPrenom()+" "+u.getNom());
								%></h3>
		  				</div>
		  				</div>
	        			<ul class="list-unstyled components mb-5">
	          				<li class="active">
	            				<a href="acceuil"><span class="fa fa-home mr-3"></span>Home</a>
	          				</li>
	          				<li>
	            				<a href="#"><span class="fa fa-user mr-3"></span> Profile</a>
	          				</li>
	          				<li>
	            				<a href="list"><span class="fa fa-users mr-3"></span> Users</a>
	          				</li>
							<li>
	            				<a href="newuser"><span class="fa fa-user-plus mr-3"></span> Add User</a>
	          				</li>
	          				<li>
	            				<a href="login"><span class="fa fa-sign-out mr-3"></span> Sign Out</a>
	          				</li>
	        			</ul>
	    	    </nav>
	    	    
	        	<!-- Page Content  -->
	      		<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Users List</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-wrap">
						<table class="table table-bordered table-dark table-hover">
						  <thead align="center">
						    <tr>
						      <th>First Name</th>
						      <th>Last Name</th>
						      <th>Category</th>
							  <th>Edit/Delete</th>
						    </tr>
						  </thead>
						  <tbody align="center">	
							<%
								List <User> usersList =  (List) request.getAttribute("listusr");
								for(User ui :usersList){ %>	
							<tr>
								<td><% out.println(ui.getPrenom());%></td>
								<td><% out.println(ui.getNom());%></td>
								<td><% out.println(ui.getEmail());%></td>
								<td><a href="#">Edit</a> | <a href="#">Delete</a></td>
							</tr>	
							<%}%>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="tabletemplate/js/jquery.min.js"></script>
  <script src="tabletemplate/js/popper.js"></script>
  <script src="tabletemplate/js/bootstrap.min.js"></script>
  <script src="tabletemplate/js/main.js"></script>
			</div>
	
	    <script type="text/javascript" src="templatefiles/js/jquery.min.js"></script>
	    <script type="text/javascript" src="templatefiles/js/popper.js"></script>
	    <script type="text/javascript" src="templatefiles/js/bootstrap.min.js"></script>
	    <script  type="text/javascript" src="templatefiles/js/main.js"></script>
	  </header>




	

	</body>
</html>


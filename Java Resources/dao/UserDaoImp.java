package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entities.User;
import metier.SingletonConnection;

public class UserDaoImp implements IUserDao {
	@Override
	public User save(User u) {
		Connection conn = SingletonConnection.getConnection();
		try {
			PreparedStatement ps = conn
					.prepareStatement("INSERT INTO Users(first_name,last_name,email,password,photo) VALUES(?,?,?,?,?)");
			ps.setString(1, u.getPrenom());
			ps.setString(2, u.getNom());
			ps.setString(3, u.getEmail());
			ps.setString(4, u.getPwd());
			ps.setString(5, u.getPhoto());
			ps.executeUpdate();
			PreparedStatement ps2 = conn.prepareStatement("SELECT MAX(id) as MAX_ID FROM Users");
			ResultSet rs = ps2.executeQuery();
			if (rs.next()) {
				u.setIdUser(rs.getLong("MAX_ID"));
			}
			ps.close();
			ps2.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return u;
	}

	@Override
	public User getUser(Long id) {
		Connection conn = SingletonConnection.getConnection();
		User u = new User();
		try {
			PreparedStatement ps = conn.prepareStatement("select * from PRODUITS where id_user = ?");
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				u.setIdUser(rs.getLong("id_user"));
				u.setPrenom(rs.getString("first_name"));
				u.setNom(rs.getString("last_name"));
				u.setEmail(rs.getString("email"));
				u.setPwd(rs.getString("password"));
				u.setPhoto(rs.getString("photo"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return u;
	}

	@Override
	public User updateUser(User u) {
		Connection conn = SingletonConnection.getConnection();
		try {
			PreparedStatement ps = conn.prepareStatement(
					"UPDATE PRODUITS SET first_name=?,last_name=?,email=?,password=?,photo=? WHERE id_user=?");
			ps.setString(1, u.getPrenom());
			ps.setString(2, u.getNom());
			ps.setString(3, u.getEmail());
			ps.setString(4, u.getPwd());
			ps.setString(5, u.getPhoto());
			ps.setLong(6, u.getIdUser());
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return u;
	}

	@Override
	public void deleteUser(Long id) {
		Connection conn = SingletonConnection.getConnection();
		try {
			PreparedStatement ps = conn.prepareStatement("DELETE FROM Users WHERE id_user = ?");
			ps.setLong(1, id);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

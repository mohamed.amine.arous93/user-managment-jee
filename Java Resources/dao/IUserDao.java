package dao;

import entities.User;

interface IUserDao {
	
	public User save(User u);

	public User getUser(Long id);

	public User updateUser(User p);

	public void deleteUser(Long id);
}
package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entities.ListUser;
import entities.User;
import services.UserService;

@WebServlet({ "/", "/UserServlet", "/ExecuterUserServlet", "/login", "/accueil", "/list", "/newuser" , "*.do"})
public class UserServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private UserService userservice;

	public UserServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getServletPath().equals("/login")) {
			String message_erreur = "";
			request.setAttribute("msgerreur", message_erreur);
			request.getRequestDispatcher("login.jsp").forward(request, response);

		} else if (request.getServletPath().equals("/list")) {
			@SuppressWarnings("unused")
			HttpSession session = request.getSession();
			@SuppressWarnings({ "unchecked", "rawtypes" })
			List<User> usersList = (List) ListUser.getAllUser();
			request.setAttribute("listusr", usersList);
			request.getRequestDispatcher("userslist.jsp").forward(request, response);

		} else if (request.getServletPath().equals("/acceuil")) {
			HttpSession session = request.getSession();
			User u = (User) session.getAttribute("user");
			session.setAttribute("user", u);
			int nbre = new UserService().GetUserCount();
			session.setAttribute("usercount", nbre);
			request.getRequestDispatcher("index.jsp").forward(request, response);

		} else if (request.getServletPath().equals("/newuser")) {
			HttpSession session = request.getSession();
			User u = (User) session.getAttribute("user");
			session.setAttribute("user", u);
			request.getRequestDispatcher("newuser.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO *Auto-generated method stub
		if (request.getServletPath().equals("/login")) {
			HttpSession session = request.getSession();
			// Récupérer login et le mot de passe depuis le formulaire "login.jsp"
			String email = request.getParameter("email").toString();
			String pwd = request.getParameter("password").toString();
			// Recherche d'un utilisateur possible à travers ces deux paramètres
			User u = new UserService().checkLogin(email, pwd);
			if (u != null) {
				// Cas 1 : utilisateur trouvé(e)
				session.setAttribute("user", u);
				session.setAttribute("nombre", this.userservice.GetUserCount());
				// en affichant le nom que le prenom de l'utilisateur
				// -> afficher renvoyer la page Accueil.jsp
				request.getRequestDispatcher("index.jsp").forward(request, response);

			} else {
				// Sinon ; Cas 2 : utilisateur non trouvé(e)
				// -> afficher de nouveau la page login.jsp
				// avec un message d'erreur :
				String message_erreur = "login introuvable";
				request.setAttribute("msgerreur", message_erreur);
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}

		} else if (request.getServletPath().equals("/newuser")) {
			String login = request.getParameter("email").toString();
			String pwd = request.getParameter("password").toString();
			String nom = request.getParameter("nom").toString();
			String prenom = request.getParameter("prenom").toString();
			String photo = request.getParameter("photo").toString();
			List<User> users = ListUser.getAllUser();
			users.add(new User(nom, prenom, login, pwd, photo));
			@SuppressWarnings({ "unchecked", "rawtypes" })
			List<User> usersList = (List) ListUser.getAllUser();
			request.setAttribute("listusr", usersList);
			request.getRequestDispatcher("userslist.jsp").forward(request, response);
		}
	}

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		this.userservice = new UserService();
	}
}
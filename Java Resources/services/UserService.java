package services;

import entities.User;

import java.util.List;

import entities.ListUser;
public class UserService {
	
	// Cette methode permet de renvoyer un objet user trouve
	// dans la collection List User 
	public User checkLogin(String vemail,String vpwd) {
		User u=null;
		List <User> users = ListUser.getAllUser();
		for(User ui :users)
		{
			if(ui.getEmail().equals(vemail) && ui.getPwd().equals(vpwd))
			{
				u=ui;
				break;
			}
		}
		return u;
	}
	
	public int GetUserCount() {
		int res=0;
		res= ListUser.getAllUser().size();
		return res;
	}
}
